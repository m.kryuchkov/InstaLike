# InstaLike
InstaLike is my just-for-fun-project. Sometimes you need to do some boring things like toggling likes on instagram. But you are hacker and hackers ~~have black terminals with green font color~~ are too lazy for it.
### Version
1.0.0
### Tech
InstaLike is written in Python 2.7 and uses [selenium](http://www.seleniumhq.org/) for maintanance.

### Installation

You need to have Python 2.7. Installation guide is [there](https://www.python.org/downloads/). 

Also you need to install selenium and chromedriver for it ([google chrome](https://www.google.ru/chrome/browser/desktop/) is also required). You can do it like that on a Mac OS:
```sh
$ sudo pip intsall selenium
$ brew install chromedriver
```
### How to use
Edit configuration in main.py:
 - username - your's account login
 - password - your's account password
 - depth - how many posts from the last look for
 - targets - what accounts you wanna likinize (list of usernames)

And start it:
```sh
$ python main.py
```
### Todos
 - Nox!

License
----
Project is publiched under MIT Standrard license.