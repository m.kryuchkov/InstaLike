#!/usr/bin/python

from instalike import InstaLike

if __name__ == "__main__":
    with InstaLike(username = '<Your login>',
                   password = '<Your password>',
                   depth = 5,
                   targets = ['nick1', 'nick2', '...']) as bot:
        bot.start()